package uvsq2;

public class Fraction {

	public int numerateur ;
	public int denuminateur ;
	final int zero =0;
	final int un=1;
	
	public Fraction(int numerateur, int denuminateur) {
		super();
		this.numerateur = numerateur;
		this.denuminateur = denuminateur;
	}

	public Fraction() {
	this.numerateur=zero;
		this.denuminateur=un;
	}

	public Fraction(int numerateur) {
		super();
		this.numerateur = numerateur;
		this.denuminateur=un;
	}

	
	public int getNumerateur() {
		return numerateur;
	}

	
	public int getDenuminateur() {
		return denuminateur;

	

	}
	
	public double vergule()
	{
		return (double) this.numerateur/this.denuminateur ;
	}
	
	@Override
	public String toString() {
		return  numerateur + "/"	+ denuminateur ;
	}
}

